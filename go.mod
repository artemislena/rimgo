module codeberg.org/video-prize-ranch/rimgo

go 1.17

require (
	github.com/aymerick/raymond v2.0.2+incompatible
	github.com/dustin/go-humanize v1.0.0
	github.com/gofiber/fiber/v2 v2.38.1
	github.com/gofiber/template v1.7.1
	github.com/joho/godotenv v1.4.0
	github.com/microcosm-cc/bluemonday v1.0.21
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/tidwall/gjson v1.14.3
	gitlab.com/golang-commonmark/linkify v0.0.0-20200225224916-64bca66f6ad3
)

require (
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/aymerick/douceur v0.2.0 // indirect
	github.com/gorilla/css v1.0.0 // indirect
	github.com/klauspost/compress v1.15.11 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.1 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.40.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/net v0.0.0-20221014081412-f15817d10f9b // indirect
	golang.org/x/sys v0.0.0-20221013171732-95e765b1cc43 // indirect
	golang.org/x/text v0.3.8 // indirect
)
